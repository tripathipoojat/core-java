package com.training.modifiers;

public class Tester {
    public static void main(String[] args) {
        ExampleAccesModifier exampleAccessModifier =new ExampleAccesModifier();
        System.out.println(exampleAccessModifier.noOfPersons);
        ExampleNonAccessModifiers.sampleStaticMethod();
        System.out.println(ExampleNonAccessModifiers.id);
        System.out.println(ExampleNonAccessModifiers.PI);

        double variableSalary=756.20;
        double salary = variableSalary + ExampleNonAccessModifiers.FIXED_SALARY;
        System.out.println(salary);
        Child child = new Child();
        child.aMethodInTheParent();

        /*ExampleNonAccessModifiers exampleNonAccessModifiers= new ExampleNonAccessModifiers();
        exampleNonAccessModifiers.printVariables();*/

    }
}
