package com.training.modifiers;

public class ExampleNonAccessModifiers {
    public static int id=108;
    public final String city = "Denver";
    public static  final double PI=3.14;
    public static final double FIXED_SALARY=1200.50;
    public static void sampleStaticMethod() {

    }
    public void printVariables(){
        System.out.println(city);

    }
}
