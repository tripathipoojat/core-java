package com.training.wrapperclasses;

public class ExampleWrapperClasses {
    public void exampleNumberClass(){
            Number number = 0;
            System.out.println(number);
            int a = number.intValue();
            System.out.println(a);
            double v = number.doubleValue();
            System.out.println(v);
        }
    }

