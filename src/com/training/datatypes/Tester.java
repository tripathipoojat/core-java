package com.training.datatypes;

public class Tester {

    public static void main(String[] args) {
        byte a = 108;
        short b =137;

        ExampleDataTypes datTypes = new ExampleDataTypes();
        datTypes.printDataTypes();


        ExampleDataTypes exampleDataTypes=new ExampleDataTypes (a,b);
        exampleDataTypes.printDataTypes();

        SalaryCalculator pooja_salary=new SalaryCalculator(1200,620.78,"Pooja");
        System.out.println(pooja_salary.calculateTotalSalary());

        SalaryCalculator bob_salary=new SalaryCalculator(1600,820.78,"Bob");
        System.out.println(bob_salary.calculateTotalSalary());
    }
}
