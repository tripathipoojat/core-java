package com.training.datatypes;

public class ExampleDataTypes {
    private byte aByte=127;
    private  short aShort=134;
    private Person person;

    public void printDataTypes(){
        System.out.println(aByte);
        System.out.println(aShort);
        System.out.println("Switched to pooja branch");
    }

    public ExampleDataTypes(){
        System.out.println("Constructor ha been invoked");
    }
    public ExampleDataTypes(byte aByte,short aShort){
        this.aByte=aByte;
        this.aShort=aShort;
    }


}
