package com.training.datatypes;

public class SalaryCalculator {
    private String name;
    private double fixedSalary;
    private double variableSalary;

    public SalaryCalculator(double fixedSalary,double variableSalary,String name) {
        this.name=name;
        this.fixedSalary = fixedSalary;
        this.variableSalary=variableSalary;
        System.out.println("calculate salary for "+name);

    }

    public double calculateTotalSalary(){
        return fixedSalary+variableSalary;
    }

}
