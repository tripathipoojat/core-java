package com.training.collections.queue;

import java.util.PriorityQueue;
import java.util.Queue;

public class ExampleQueue {
    Queue<String> peopleQueue = new PriorityQueue<>(3);

    public void printQueue(){

        peopleQueue.add("Ram");
        peopleQueue.add("Sam");
        peopleQueue.add("Gopi");
        peopleQueue.add("Eva");
        peopleQueue.offer("Somu");



        System.out.println(peopleQueue);
        System.out.println(peopleQueue.peek());
        System.out.println(peopleQueue.remove());
        System.out.println(peopleQueue);
        System.out.println(peopleQueue.poll());
        System.out.println(peopleQueue.element());
        System.out.println(peopleQueue);

       /* Queue queue= new PriorityQueue();
        System.out.println(queue.poll());*/
        if (peopleQueue.iterator().hasNext()){
            System.out.println(peopleQueue.iterator().next());
        }

    }
}
