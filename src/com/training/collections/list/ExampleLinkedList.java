package com.training.collections.list;

import java.util.LinkedList;

public class ExampleLinkedList {
    LinkedList<Integer> sequentialList = new LinkedList<>();

    public void printLinkedList() {
        sequentialList.add(1002);
        sequentialList.add(9999);
        sequentialList.add(1008);
        sequentialList.add(9876);
        sequentialList.add(5469);
        sequentialList.add(3658);
        System.out.println(sequentialList);
        System.out.println(sequentialList.getFirst());
        System.out.println(sequentialList.getLast());
        System.out.println(sequentialList.remove());
        System.out.println(sequentialList);


    }
}
