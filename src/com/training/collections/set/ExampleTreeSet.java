package com.training.collections.set;

import java.util.*;

public class ExampleTreeSet {
    List<Integer> list = new ArrayList<>();

    SortedSet<Integer> sortedSet = new TreeSet<>();

    public void printTreeSet() {

        list.add(3444);
        list.add(433);
        list.add(433);
        list.add(8394454);
        list.add(65);
        list.add(8394454);
        list.add(65);
        list.add(53);
        list.add(65);
        list.add(65);
        list.add(35);
        list.add(35);

        SortedSet<Integer> integerSortedSet = new TreeSet<>(list);
        System.out.println(list);
        System.out.println(integerSortedSet);

        sortedSet.add(2900);
        sortedSet.add(1001);
        sortedSet.add(8394);
        sortedSet.add(9999);
        sortedSet.add(4999);
        sortedSet.add(1993);
        System.out.println(sortedSet);

        /*while (sortedSet.iterator().hasNext()) {
            System.out.println(sortedSet.iterator().next());
        }*/

        Person ram = new Person(1001,24,"Ram",10062.99,"N");
        Person sam = new Person(1002,32,"Sam",6062.99,"K");
        Person max = new Person(1003,67,"Max",100632.99,"C");
        Person rat = new Person(1004,72,"RATAN",10062998.99,"C");
        Person gun = new Person(1005,54,"Gun",109062.99,"C");
        List<Person> people = new ArrayList<>();
        people.add(ram);
        people.add(sam);
        people.add(max);
        people.add(rat);
        people.add(gun);
        System.out.println(people);

        /*Collections.sort(people, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getName() > o2.getName();
            }
        });
*/
        Collections.sort(people, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        /*Collections.sort(people, new SortByName());*/
        System.out.println(people);

    }
}

class SortByName implements Comparator<Person>{
    public int compare(Person a, Person b) {
        return a.getName().compareTo(b.getName());
    }
}
