package com.training.collections.set;

public class Tester {
    public static void main(String[] args) {

        ExampleHashSet set = new ExampleHashSet();
        set.playWithSet();
        ExampleTreeSet treeSet = new ExampleTreeSet();
        treeSet.printTreeSet();
    }
}
