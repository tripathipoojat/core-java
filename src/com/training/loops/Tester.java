package com.training.loops;

public class Tester {
    public static void main(String[] args) {
        ExampleLoops exampleLoops=new ExampleLoops();
        exampleLoops.genericForLoop();
        exampleLoops.exampleForEachLoop();
        exampleLoops.exampleWhileLoop();
        exampleLoops.exampleDoWhileLoop();
        ExampleLoppsControl exampleLoppsControl=new ExampleLoppsControl();
        exampleLoppsControl.printLoopsControls();
    }
}
