package com.training.loops;

public class ExampleLoops {

    private int ids[]={1001,1002,1003,1004,1005,1102,1208,1562};
    private int x = 20;
    public void genericForLoop(){
        for (int i=0; i<=10;i++){
            System.out.println();
        }
    }
    public void exampleForEachLoop(){
        System.out.println("***********Example for each loop******");
        for(int id:ids){
            System.out.println(id);
        }
    }
    public void exampleWhileLoop(){
        while(x>0){
            System.out.println(x);
            x--;
        }
    }
    public void exampleDoWhileLoop() {
        do {
            System.out.println(x);
            x++;
        }
        while (x <= 40);

        }

    }

