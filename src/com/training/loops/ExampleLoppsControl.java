package com.training.loops;

public class ExampleLoppsControl {
    public void printLoopsControls(){
        System.out.println("**********Loop Controls**********");
        for (int x =0; x < 50 ; x++){
            if(x == 20){
                break;
            }
            System.out.println(x);
        }
        System.out.println(">>>>>>>>>>>>>>>Out side for break<<<<<<<<<<<<<<<");

        for (int x =0; x < 20 ; x++){
            if(x == 10){
                continue;
            }
            System.out.println(x);
        }
        System.out.println(">>>>>>>>>>>>>>>Out side for continue<<<<<<<<<<<<<<<");


    }
}
