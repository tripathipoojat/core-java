package com.training.oops.project;

public class Customer extends Person{
    private long accNumber;
    private String address;
    private double balance;
    private boolean isAccountActive;

    public Customer(String name, int age, long mobileNumber, String emailId,double balance) {
        this.balance=balance;
    }

    public long getAccNumber() {
        return accNumber;
    }

    public void setAccNumber(long accNumber) {
        this.accNumber = accNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public boolean isAccountActive() {
        return isAccountActive;
    }

    public void setAccountActive(boolean accountActive) {
        isAccountActive = accountActive;
    }
    public String withDrawMoney(double balance,double withDrawl){
        if ( balance > withDrawl){
            return "WithDrawl Success";

        }else{
            return "WithDrawl Success";
        }

    }

    public double depositMoney(double balance,double depositAmount){
        return balance+depositAmount;

    }
}
