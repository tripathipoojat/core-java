package com.training.oops.project;

public class Employee extends Person{
    private long employeeId;
    private String department;

    public Employee(String name, int age, long mobileNumber, String emailId,long employeeId) {

        this.employeeId=employeeId;
    }

    public long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(long employeeId) {
        this.employeeId = employeeId;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
}
