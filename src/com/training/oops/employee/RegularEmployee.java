package com.training.oops.employee;

public class RegularEmployee extends Employee {


    private double fixedComponent = 2500D;
    private double variableComponent = 1500D;
    private double insuranceCost = 500;
    private double holidayAllowance = 1200;

    @Override
    public double calculateSalary() {
        return fixedComponent + variableComponent + holidayAllowance + insuranceCost;
    }
}

