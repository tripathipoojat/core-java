package com.training.oops.employee;

public class DailyWageEmployee extends Employee {

    private double wagePerHour = 50.00D;
    private float noOfHours = 8;


    @Override
    public double calculateSalary() {
        return noOfHours * wagePerHour;
    }
}
