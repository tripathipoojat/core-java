package com.training.oops.employee;

public abstract class Employee {

    public Employee() {
    }

    public abstract double calculateSalary();
}
