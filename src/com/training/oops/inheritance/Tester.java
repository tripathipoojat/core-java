package com.training.oops.inheritance;

public class Tester {
    public static void main(String[] args) {

        Child child = new Child("John", 1001, true);
        child.methodFromParent();
        child.thisIsAMethodFromExampleParent();

        Parent parent = new Child();
        parent.methodFromParent();

        /*
         * */
        Animal animal = new Animal();
        Mammal mammal = new Mammal();
        Deer deer = new Deer();

        System.out.println(mammal instanceof Animal);
        System.out.println(deer instanceof Mammal);
        System.out.println(deer instanceof Animal);

        ExampleStaticMethod.sampleMethod();

    }
}

