package com.training.oops.inheritance;

    public class Parent extends ExampleParent{

        private String name;
        private int id;
        private boolean isGood;

        public Parent(String name, int id, boolean isGood) {
            this.name = name;
            this.id = id;
            this.isGood = isGood;
            System.out.println("you invoked the parent class constructor with parameters");

        }

        public Parent() {
            System.out.println("you invoked the parent class constructor");
        }

        public void methodFromParent() {
            System.out.println("This is a method from parent class");
        }
    }

