package com.training.oops.Interface;


import java.io.IOException;

public interface OurFirstInterface {

    int X = 0;



    int sum();
    double getAge() throws IOException;

    static void saySomething()  {
        System.out.println("");
    }

}
