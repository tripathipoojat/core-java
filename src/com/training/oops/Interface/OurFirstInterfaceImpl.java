package com.training.oops.Interface;

import java.io.IOException;

public class OurFirstInterfaceImpl implements InterfaceThree{


    @Override
    public int sum() {
        return 0;
    }

    @Override
    public double getAge() throws IOException {
        return 0;
    }


    @Override
    public String getStatus() {
        return null;
    }
}
