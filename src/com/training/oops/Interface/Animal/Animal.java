package com.training.oops.Interface.Animal;

public interface Animal {

    void eat();

    void walk();

    void run();
}
