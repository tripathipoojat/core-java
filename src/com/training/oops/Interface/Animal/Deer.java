package com.training.oops.Interface.Animal;

public class Deer implements Mammal{

    @Override
    public void eat() {

    }

    @Override
    public void walk() {

    }

    @Override
    public void run() {

    }

    @Override
    public String givesMilk() {
        return null;
    }

    public boolean hasHorns(){
        return true;
    }

}
