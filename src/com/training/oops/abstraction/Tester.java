package com.training.oops.abstraction;


import com.training.oops.employee.DailyWageEmployee;
import com.training.oops.employee.Employee;
import com.training.oops.employee.RegularEmployee;

public class Tester {
    public static void main(String[] args) {

        ExampleAbstractClass exampleAbstractClass = new ExampleAbstractImplementation();
        exampleAbstractClass.exampleAbstractMethod();
        OneMoreImplementation oneMoreImplementation = new OneMoreAbstractImplementation();

        Employee dailyWageEmployee = new DailyWageEmployee();
        System.out.println(dailyWageEmployee.calculateSalary() + " per day");
        Employee regulaEmployee = new RegularEmployee();
        System.out.println(regulaEmployee.calculateSalary() + " per month");
    }
}