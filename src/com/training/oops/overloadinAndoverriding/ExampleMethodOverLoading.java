package com.training.oops.overloadinAndoverriding;

public class ExampleMethodOverLoading {
    public int sum(int a, int b) {
        return a + b;
    }

    public double sum(double x, double y) {
        return x + y;
    }

    public int sum(int a, int b, int c) {
        return a + b + c;
    }
}
