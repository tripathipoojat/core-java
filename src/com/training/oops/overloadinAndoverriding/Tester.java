package com.training.oops.overloadinAndoverriding;

public class Tester {
    public static void main(String[] args) {

        int a = 10, b = 45, c = 52;
        double x = 56.36, y = 96.33;
        ExampleMethodOverLoading methodOverLoading = new ExampleMethodOverLoading();
        System.out.println(methodOverLoading.sum(a, b));
        System.out.println(methodOverLoading.sum(a, b, c));
        System.out.println(methodOverLoading.sum(x, y));
        Square square = new Square();
        Square circle = new Circle();
        System.out.println(square.calculateTheArea());
        System.out.println(circle.calculateTheArea());
        System.out.println(Circle.someMethod());
        System.out.println(Square.someMethod());

    }

}
