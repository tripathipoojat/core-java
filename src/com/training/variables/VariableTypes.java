package com.training.variables;

import com.training.modifiers.ExampleAccesModifier;

import java.sql.SQLOutput;

public class VariableTypes extends ExampleAccesModifier {
    String name = "VariableTypes";
    public static String COUNTRYCODE="USA";


    public void printLocalVariables(){
        int a=10;
        System.out.println(a);
        System.out.println(name);
        System.out.println(COUNTRYCODE);
    }
    public void secondMethod(){
        System.out.println(name);
    }
}
