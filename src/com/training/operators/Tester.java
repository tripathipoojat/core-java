package com.training.operators;

public class Tester {
    public static void main(String[] args) {
        int x = 30,y = 20;
        double a = 12.09 , b = 67.09;
        ExampleAirthmeticOperations exampleAirthmeticOperations=new ExampleAirthmeticOperations();
        System.out.println("addition of "+ x + " and " +y+ " is "+exampleAirthmeticOperations.sum(x,y));
        System.out.println("subtraction of "+ x + " and " +y+ " is "+exampleAirthmeticOperations.subtraction(x,y));
        System.out.println("multiplication of "+ x + " and " +y+ " is "+exampleAirthmeticOperations.multiplication(x,y));
        System.out.println("division of "+ x + " and " +y+ " is "+exampleAirthmeticOperations.division(x,y));
        System.out.println("modulus of "+ x + " and " +y+ " is "+exampleAirthmeticOperations.modulus(x,y));
        System.out.println("increment of "+ x + " and is "+exampleAirthmeticOperations.increment(x));
        System.out.println("decrement of "+ x + " and is "+exampleAirthmeticOperations.decrement(x));

        System.out.println("*******************Relational Operation Started**********");
        ExampleRelationalOperation exampleRelationalOperation=new ExampleRelationalOperation();
        System.out.println(exampleRelationalOperation.equals(a,b));

        System.out.println("********************Assignment Operators started***************");
        ExampleAssignmentOperators exampleAssignmentOperators= new ExampleAssignmentOperators();
        exampleAssignmentOperators.printVariables();

    }



}
