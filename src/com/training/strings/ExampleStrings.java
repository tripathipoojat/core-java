package com.training.strings;

public class ExampleStrings {

    private String name = "Peter parker";
    char fruitChar[] = {'A', 'P', 'P', 'L', 'E'};

    String fruit = new String(fruitChar);

    String city = new String("Newyork");
    private String str = "nEwYoRk";
    private Object object;

    private String emptyStr = " ";

    public void printStrings() {

        System.out.println(name);
        System.out.println(fruitChar);
        System.out.println(fruit);
        System.out.println(city);
        System.out.println(name.length());
        /*System.out.println(str.isEmpty());*/
        System.out.println(city.charAt(3));
        System.out.println(name.compareTo(fruit));
        System.out.println(city.compareToIgnoreCase(str));

    }

    public void printConcatinatedStrings(){
        String conStr = city + name;
        String conStr1 = city.concat(name);
        String conStr2 = city +", eee USA";
        String st ="Hello";
        System.out.println(conStr);
        System.out.println(conStr1);
        System.out.println(conStr2);
        System.out.println("Hello "+ name+", How are you doing in "+city );
        System.out.println(conStr1.getBytes());
        System.out.println(conStr2.hashCode());
        System.out.println(conStr1.substring(4));
        System.out.println(conStr1.substring(4,9));
        System.out.println(emptyStr.isEmpty());
        System.out.println(emptyStr.isBlank());
        System.out.println(city.charAt(4));
        System.out.println(city.codePointAt(0));
        System.out.println(conStr2.toLowerCase());
        System.out.println(conStr2.replace('e','m'));
        System.out.println(conStr2.toUpperCase());
        char[] repArr = st.toCharArray();
        repArr[4] = 'a';
        System.out.println(repArr);

    }

    public void someMethod(Integer a){}


    public void exampleStringBuilderAndStringBuffer(){

        StringBuilder builder = new StringBuilder("You must cross a course cross cow");
        StringBuilder stringBuilder = new StringBuilder("You must cross a course cross cow");
        System.out.println(builder.append("with grass"));
        System.out.println(builder.replace(7,10,"man"));
        System.out.println(builder.reverse());
        System.out.println(builder.delete(5,11));
        System.out.println(builder.compareTo(stringBuilder));
        builder.toString();

    }
}

