package com.training.decisionmaking;

public class ExampleDecisionMaking {
    private String[] students = {"Rohan", "Peter", "Max", "Ben", "Prakhar", "Rose"};
    private int[] marks = {95, 76, 89, 62, 98, 55};

    public int  maximumMarks(int score){
        int maxNum = score;
        for (int number:marks){
            if (number>maxNum){
                maxNum=number;
            }
        }
        return maxNum;
    }


    public String studentNameWithMarks(String name,int score) {
        for (String student : students) {
            if (name == student) {
                System.out.println(name + " is student of school ");
                break;

            }
        }
        for (int mark : marks) {
            if (score == mark) {
                System.out.println(score + " are marks of " +name);
             break;
            }
        }
        System.out.println("Given name is in not in list");
        return ("");

    }
    public void isGreaterOrSmallerThan(){
        int x = 5;
        if(x <16){
            System.out.println(x+" is smaller than 16");
        }
        else{
            System.out.println(x+" is greater than 16");

        }

    }
    public void isDivisible(int x) {
        if (x % 2 == 0) {
            System.out.println(x + " is divisible by 2");
            if (x % 3 == 0) {
                System.out.println(x + " is divisible by 3");
            } else {
                System.out.println(x + " is not divisible by 3");
            }

            }else{
            System.out.println(x + " is not divisible by 2");
            if (x % 3 == 0) {
                System.out.println(x + " is divisible by 3");
            }
        }


    }}