package com.training.decisionmaking;

public class ExampleSwitchCase {
    public String getRemarksOnGrade(char grade){
        switch(grade){
            case 'A':
                return "Excellent";
            case 'B':
                return "Very Good";
            case 'C':
                return "Good";
            case 'D':
                return "Fair";
            case 'E':
                return "Fail";
        }
        return "";

    }
    public void exampleTerenaryOperator(int a) {
        boolean isEvenNumber;

        isEvenNumber = (a%2 ==0)? true: false;

        System.out.println(isEvenNumber);
    }
}
